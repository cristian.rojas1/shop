#SHOP
--------
<ol>
    <li>compras por usuario</li>
    <li>2 vistas html</li>
    <ol>
        <li>listado de los usuarios existentes y campo de creacion de nuevos</li>
        <li>listado de compra de instancia de usuario, asi como campo de agregacion de compra(debe de tener 2 inputs)</li>
    </ol>
    <li>clase persona</li>
    <ol>
        <li>propiedades:</li>
        <ol>
            <li>array items, contiene las compras</li>
            <li>cada item debe de tener debe de tener dos propiedades (name, quantity)</li>
        </ol>
        <li>funciones:</li>
        <ol>
            <li>add(int){{agrega una cantidad de elementos}}</li>
            <li>remove(quantity){eliminar una cantidad de elementos}</li>
        </ol>
    </ol>
    <li>se debe utilizar formato JSON para enviar informacion entre ficheros</li>
</ol>

##Criterios de aceptacion
---------
<ol>
    <li>Utilizar mínimo 2 HTML (dos vistas) y 2 JS (Clase Persona y Control de Usuarios).</li>
    <li>Utilizar los tipos de datos mencionados, array, clase, objeto, etc.</li>
    <li>No utilizar var, pues está deprecated, entender la diferencia de let y const.</li>
</ol>
